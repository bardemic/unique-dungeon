﻿using System;
using scene_tools;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace generics
{
    public class Enemy : Mover
    {
        // Experience
        public int xpValue = 1;
    
        // Logic
        public float triggerLength = 1;
        public float chaseLength = 2;
        protected bool chasing;
        private bool _collidingWithPlayer;
        private Transform _playerTransform;
        protected Vector3 startingPosition;
        protected bool runAtPlayer = true;
        public string type = "";
        public string family = "any";
    
        // Hitbox
        public ContactFilter2D filter;
        private BoxCollider2D _hitbox;
        private Collider2D[] _hits = new Collider2D[10];

        protected override void ReceivedDamage(Damage dmg)
        {
            dmg.pushForce *= 3;
            base.ReceivedDamage(dmg);
        }

        protected override void Start()
        {
            base.Start();
            _playerTransform = GameManager.instance.player.transform;
            startingPosition = transform.position;
            _hitbox = transform.GetChild(0).GetComponent<BoxCollider2D>();
        }

        protected virtual void FixedUpdate()
        {
            // Is the player in range
            if (Vector3.Distance(_playerTransform.position, startingPosition) < chaseLength)
            {
                if (Vector3.Distance(_playerTransform.position, startingPosition) < triggerLength)
                    chasing = true;
            
                if (chasing)
                {
                    if (!_collidingWithPlayer)
                    {
                        if (runAtPlayer)
                            UpdateMotor((_playerTransform.position-transform.position).normalized);
                        else
                            UpdateMotor((_playerTransform.position-transform.position).normalized*-1);
                    }
                }
                else
                {
                    UpdateMotor(startingPosition - transform.position);
                }

            }
            else
            {
                UpdateMotor(startingPosition - transform.position);
                chasing = false;
            }
        
            // Check for overlaps
            _collidingWithPlayer = false;
            boxCollider.OverlapCollider(filter, _hits);
            for (var i = 0; i < _hits.Length; i++)
            {
                if (_hits[i]==null)
                    continue;
                if (_hits[i].CompareTag("Player") && _hits[i].name == "player")
                {
                    _collidingWithPlayer = true;
                }
            
                // clean up
                _hits[i] = null;
            }
        }

        protected override void Death()
        {
            GameManager.instance.weaponManager.KilledEnemy(type);
            Destroy(gameObject);
            GameManager.instance.GrantXp(xpValue);
            GameManager.instance.ShowText("+"+xpValue + " xp", 30, Color.green, transform.position, Vector3.up * 40, 1.0f);
        }

        public struct GenerationData
        {
            public int xpValue;
            public Tilemap grid;
            public int hp;
            public int damage;
        }
        
        public virtual void Generate(GenerationData data)
        {
            xpValue = data.xpValue;
            maxHitPoint = hitPoint = data.hp;
            if (TryGetComponent(out EnemyHitbox hitbox))
            {
                hitbox.damage = data.damage;
            }
        }
    }
}
