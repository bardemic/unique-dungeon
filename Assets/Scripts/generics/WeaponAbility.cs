﻿using System.Collections;
using System.Security.Cryptography;
using Unity.Jobs;
using UnityEngine;

namespace generics
{
    public class WeaponAbility: MonoBehaviour
    {
        public Damage damage;
        public int dotDamage;
        public int dotMagicDamage;
        public int dotPhysicalDamage;
        public float dotLength;
        public string enemyType;
        public Damage bonusDamage;
        public bool dealBonusDamage;

        public WeaponAbility()
        {
            damage = new Damage(Vector3.zero, 1, 0f, 0, 0);
        }

        public WeaponAbility(Damage damage)
        {
            this.damage = damage;
        }

        public WeaponAbility(Damage damage, Damage bonusDamage)
        {
            this.damage = damage;
            this.bonusDamage = bonusDamage;
        }
        
        
        public void OnHit(Collider2D coll, Vector3 origin)
        {
            damage.origin = origin;
            if(dealBonusDamage && coll.gameObject.TryGetComponent(out Enemy component) && component.type == enemyType)
                coll.SendMessage("ReceivedDamage", damage + bonusDamage);
            else
            {
                coll.SendMessage("ReceivedDamage", damage);
            }

            if (dotDamage > 0)
                StartCoroutine(DotDamage(coll));

        }
        
        private IEnumerator DotDamage(Collider2D coll)
        {
            var tick = Time.time;
            var start = 0f;
            var dmg = new Damage()
            {
                origin = Vector3.zero,
                damageAmount = dotDamage,
                magicDmg = dotMagicDamage,
                physicalDmg = dotPhysicalDamage,
                pushForce = 0
            };

            while (start < dotLength)
            {
                if(coll.gameObject == null)
                    break;
                start += Time.deltaTime;
                if (Time.time - tick >= 1)
                    coll.SendMessage("ReceivedDamage", dmg);
                yield return null;
            }

        }
    }
}