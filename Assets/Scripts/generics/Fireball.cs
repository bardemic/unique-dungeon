﻿using UnityEngine;
using UnityEngine.Diagnostics;

namespace generics
{
    public class Fireball : Projectile
    {
        private Vector3 _direction;

        protected override void Start()
        {
            base.Start();
            killedBySword = true;
        }

        protected override void FixedUpdate()
        {
            if (!gameObject.activeSelf) return;
            transform.position += _direction * -moveSpeed.x * Time.deltaTime;
            CheckBockingCollision();
        }
        
        public void SetDirection()
        {
            _direction = (transform.position - targetPos).normalized;
            var n = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg;
            if (n < 0) n += 360;
            transform.eulerAngles = new Vector3(0, 0, n);
            transform.Rotate(0,0,-90);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            _direction = Vector2.zero;
        }
    }
}