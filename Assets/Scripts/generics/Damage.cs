using UnityEngine;

namespace generics
{
    public struct Damage
    {
        public Vector3 origin;
        public int damageAmount;
        public float pushForce;
        public int magicDmg;
        public int physicalDmg;
        
        public Damage(Vector3 origin, int dmgAmount, float knockBack, int magicDmg, int physicalDmg)
        {
            if (magicDmg + physicalDmg > dmgAmount)
                this.damageAmount = magicDmg + physicalDmg;
            else
                this.damageAmount = dmgAmount;
            pushForce = knockBack;
            this.magicDmg = magicDmg;
            this.physicalDmg = physicalDmg;
            this.origin = origin;
        }

        public static Damage operator +(Damage a, Damage b) => new()
        {
            origin = a.origin,
            damageAmount = a.damageAmount + b.damageAmount,
            pushForce = a.pushForce + b.pushForce,
            magicDmg = a.magicDmg + b.magicDmg,
            physicalDmg = a.physicalDmg + b.physicalDmg
        };
        
        public static Damage operator -(Damage a, Damage b) => new()
        {
            origin = a.origin,
            damageAmount = a.damageAmount - b.damageAmount,
            pushForce = a.pushForce - b.pushForce,
            magicDmg = a.magicDmg - b.magicDmg,
            physicalDmg = a.physicalDmg - b.physicalDmg
        };
    }
}
