using System.Collections;
using System.Collections.Generic;
using generics;
using scene_tools;
using UnityEngine;

public class Fighter : MonoBehaviour
{
    public int hitPoint = 10;
    public int maxHitPoint = 10;
    public float pushRecoverySpeed = 0.2f;
    protected int magicResist = 0;
    protected int physicalResist = 0;
    
    // Immunity
    protected float immuneTime = 1.0f;
    protected float lastImmune;
    
    // Push
    protected Vector3 pushDirection;

    protected virtual void ReceivedDamage(Damage dmg)
    {
        if (Time.time - lastImmune > immuneTime)
        {
            lastImmune = Time.time;
            hitPoint -= dmg.damageAmount;
            pushDirection = (transform.position - dmg.origin).normalized * dmg.pushForce;

            GameManager.instance.ShowText(dmg.damageAmount.ToString(), 35, Color.red, transform.position, Vector3.zero, .5f);
            
            if (hitPoint <= 0)
            {
                hitPoint = 0;
                Death();
            }
        }
    }

    protected virtual void Death()
    {
        
    }
}
