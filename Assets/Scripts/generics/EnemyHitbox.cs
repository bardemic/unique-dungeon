using UnityEngine;

namespace generics
{
    public class EnemyHitbox : Collidable
    {
        // Damage
        public int damage = 1;
        public float pushForce = 2;

        protected override void OnCollide(Collider2D coll)
        {
            if (coll.CompareTag("Player"))
            {
                var dmg = new Damage
                {
                    damageAmount = damage,
                    origin = transform.position,
                    pushForce = pushForce,
                    magicDmg = 0,
                    physicalDmg = 0
                };
            
                coll.SendMessageUpwards("ReceivedDamage", dmg);
            }
        }
    }
}
