﻿using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace generics
{
    public class DropCoinsOnDeath : MonoBehaviour
    {
        public GameObject coinPrefab;
        public GameObject coinParent;
        public int coinsDropped = 1;
        private Vector3 _pos;
        
        private void OnDisable()
        {
            _pos = transform.position;
            for (var i = 0; i < coinsDropped; i++) 
            { 
                Instantiate(coinPrefab, _pos, Quaternion.identity, coinParent.transform);
            }
        }
    }
}