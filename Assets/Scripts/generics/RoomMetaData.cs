﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace generics
{
    public class RoomMetaData : MonoBehaviour
    {
        public struct DoorLayout
        {
            public bool top;
            public bool left;
            public bool right;
            public bool bottom;

            public bool Compare(DoorLayout other)
            {
                return top == other.top && left == other.left && right == other.right && bottom == other.bottom;
            }
        }

        // Meta Data
        private DoorLayout _layout;
        [Range(0,3)] public int tileType = 0;
        public bool isRandom = true;
        public Tuple<int, int> pos;
        public bool isSpawn;
        public bool isBoss;
        public bool mobSpawn;
        public bool loot;
        public Tilemap spawnMap;

        // Serialized special data
        [SerializeField] private bool layoutTop;
        [SerializeField] private bool layoutRight;
        [SerializeField] private bool layoutLeft;
        [SerializeField] private bool layoutBottom;

        // Constants
        public const int TileDungeon = 0;
        public const int TileDessert = 1;
        public const int TileSmooth = 2;
        public const int TileJungle = 3;

        public static List<int> AllTileTypes()
        {
            return new List<int>{TileDessert, TileDungeon, TileJungle, TileSmooth};
        }

        public static string GetTileType(int type)
        {
            return type switch
            {
                TileDessert => "dessert",
                TileDungeon => "dungeon",
                TileSmooth => "smooth",
                TileJungle => "jungle",
                _ => ""
            };
        }

        private void Start()
        {
            _layout.bottom = layoutBottom;
            _layout.top = layoutTop;
            _layout.left = layoutLeft;
            _layout.right = layoutRight;
        }

        public DoorLayout GetLayout()
        {
            _layout.bottom = layoutBottom;
            _layout.top = layoutTop;
            _layout.left = layoutLeft;
            _layout.right = layoutRight;
            return _layout;
        }
    }
}