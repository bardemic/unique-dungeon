using System.Collections;
using System.Collections.Generic;
using scene_tools;
using UnityEngine;

public class NPC : Collidable
{
    public string message;
    public Color color = Color.white;
    public int size = 25;

    private float lastShout = -4f;
    private float cooldown = 4f;
    protected override void OnCollide(Collider2D coll)
    {
        if (Time.time - lastShout > cooldown & coll.CompareTag("Player"))
        {
            lastShout = Time.time;
            GameManager.instance.ShowText(message,size, color, transform.position+new Vector3(0,.16f,0), Vector3.zero, 4f);
        }
    }
}
