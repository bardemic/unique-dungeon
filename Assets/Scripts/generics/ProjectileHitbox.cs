﻿using UnityEngine;

namespace generics
{
    [RequireComponent(typeof(Projectile), typeof(BoxCollider2D))]
    public class ProjectileHitbox : EnemyHitbox
    {
        private Projectile _projectile;
        protected override void Start()
        {
            base.Start();
            _projectile = GetComponent<Projectile>();
        }

        protected override void OnCollide(Collider2D coll)
        {
            base.OnCollide(coll);
            if (!coll.CompareTag("Player")) return;
            _projectile.active = false;
            gameObject.SetActive(_projectile.active);
        }
    }
}