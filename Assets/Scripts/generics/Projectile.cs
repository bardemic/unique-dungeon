﻿using System;
using Unity.Collections;
using UnityEngine;

namespace generics
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class Projectile : MonoBehaviour
    {
        [NonSerialized] public Vector3 targetPos;
        private RaycastHit2D hit;
        protected BoxCollider2D boxCollider;
        public bool active = false;
        public Vector2 moveSpeed = new Vector3(1,1);
        public bool disableOnStill = true;
        public float timeShot = 0f;
        private ContactFilter2D _filter;
        private Collider2D[] _hits = new Collider2D[10];
        protected bool killedBySword = false;

        protected virtual void Start()
        {
            boxCollider = GetComponent<BoxCollider2D>();
        }

        protected virtual void FixedUpdate()
        {
            if (gameObject.activeSelf)
            {
                UpdateMotor();
            }
        }

        protected virtual void UpdateMotor()
        {
            var moveDelta = (targetPos-transform.position).normalized;
            moveDelta = new Vector3(moveDelta.x * moveSpeed.x, moveDelta.y * moveSpeed.y, 0);
            transform.Translate(moveDelta*Time.deltaTime);
            
            if(disableOnStill && moveDelta == Vector3.zero)
                ChangeActive(false);
        }

        protected void CheckBockingCollision()
        {
            boxCollider.OverlapCollider(_filter, _hits);
            for (var i = 0; i < _hits.Length; i++)
            {
                if (_hits[i]==null)
                    continue;
                if(_hits[i].name == "Collision")
                    ChangeActive(false);
                if(killedBySword & _hits[i].CompareTag("weapon"))
                    ChangeActive(false);
            
                // clean up
                _hits[i] = null;
            }
        }

        protected Collider2D HitPlayer()
        {
            boxCollider.OverlapCollider(_filter, _hits);
            for (var i = 0; i < _hits.Length; i++)
            {
                if (_hits[i]==null)
                    continue;
                if(_hits[i].CompareTag("Player"))
                    return _hits[i];

                // clean up
                _hits[i] = null;
            }

            return null;
        }
        
        protected Collider2D HitPlayer(Collider2D coll)
        {
            coll.OverlapCollider(_filter, _hits);
            for (var i = 0; i < _hits.Length; i++)
            {
                if (_hits[i]==null)
                    continue;
                if(_hits[i].CompareTag("Player"))
                    return _hits[i];

                // clean up
                _hits[i] = null;
            }

            return null;
        }

        public virtual void ChangeActive(bool a)
        {
            active = a;
            gameObject.SetActive(active);
        }

        protected virtual void OnDisable()
        {
            active = false;
        }

        protected virtual void OnEnable()
        {
            active = true;
            timeShot = Time.time;
        }
    }
}