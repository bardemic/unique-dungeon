using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Mover : Fighter
{
    private Vector3 originalSize;
    public BoxCollider2D boxCollider;
    private RaycastHit2D hit;
    private Vector3 moveDelta;
    protected bool isWalking = false;

    public float ySpeed = .75f;
    public float xSpeed = .75f;
    
    // Start is called before the first frame update
    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        originalSize = transform.localScale;
    }

    protected virtual void UpdateMotor(Vector3 input)
    {
        isWalking = false;
        // Reset MoveDelta
        moveDelta = new Vector3(input.x * xSpeed, input.y * ySpeed, 0);
        
        // Swap sprite direction
        FlipSprite(moveDelta);

        // Add push vector
        moveDelta += pushDirection;
        
        // reduce push based on recovery
        pushDirection = Vector3.Lerp(pushDirection, Vector3.zero, pushRecoverySpeed);

        hit = Physics2D.BoxCast(boxCollider.transform.position, boxCollider.size, 0,
            new Vector2(0, moveDelta.y), Mathf.Abs(moveDelta.y* Time.deltaTime),
            LayerMask.GetMask("Actor", "Blocking"));
        if (hit.collider == null)
        {
            // Movement y
            transform.Translate(0, moveDelta.y * Time.deltaTime, 0);
            if(moveDelta != Vector3.zero)
                isWalking = true;
        }
        
        hit = Physics2D.BoxCast(boxCollider.transform.position, boxCollider.size, 0,
            new Vector2(moveDelta.x, 0), Mathf.Abs(moveDelta.x* Time.deltaTime),
            LayerMask.GetMask("Actor", "Blocking"));
        if (hit.collider == null)
        {
            // Movement x
            transform.Translate(moveDelta.x * Time.deltaTime, 0, 0);
            if(moveDelta != Vector3.zero)
                isWalking = true;
        }
    }

    protected virtual void FlipSprite(Vector3 moveDeltaVector3)
    {
        if(moveDelta.x>0)
            transform.localScale = originalSize;
        else if (moveDelta.x < 0)
            transform.localScale = new Vector3(originalSize.x*-1, originalSize.y, originalSize.z);
    }
}
