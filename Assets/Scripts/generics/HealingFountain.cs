using System.Collections;
using System.Collections.Generic;
using scene_tools;
using UnityEngine;

public class HealingFountain : Collidable
{
    public int healingAmount = 1;

    private float healCooldown = 1f;
    private float lastheal;

    protected override void OnCollide(Collider2D coll)
    {
        if(!coll.CompareTag("Player"))
            return;
        if (Time.time - lastheal > healCooldown)
        {
            lastheal = Time.time;
            GameManager.instance.player.Heal(healingAmount);
        }
    }
}
