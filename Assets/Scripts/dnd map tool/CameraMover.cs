using UnityEngine;

namespace dnd_map_tool
{
    public class CameraMover : MonoBehaviour
    {
        public float moveSpeed = 1;
        public Canvas can;

        // Update is called once per frame
        private void Update()
        {
            transform.Translate(Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime,
                Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime, 0);
            if(Input.GetKeyDown(KeyCode.Escape))
                can.gameObject.SetActive(true);
        }
    }
}
