using System;
using scene_tools;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace dnd_map_tool
{
    public class DataChanger : MonoBehaviour
    {
        public LevelGeneratorManager generator;

        public Slider minRooms;
        public Slider curveOrder;
        public Slider deadMax;
        public Slider deadMultiple;
        public TextMeshProUGUI minRoomTxt, curveOrderTxt, deadMaxTxt, deadMultipleTxt;
        
        // Start is called before the first frame update
        private void Start()
        {
            minRooms.value = generator.minRooms;
            minRoomTxt.text = generator.minRooms.ToString();
            curveOrder.value = generator.hilbertCurveOrder;
            curveOrderTxt.text = generator.hilbertCurveOrder.ToString();
            deadMax.value = generator.deadEndMax;
            deadMaxTxt.text = generator.deadEndMax.ToString();
            deadMultiple.value = generator.deadEndMultiple;
            deadMultipleTxt.text = generator.deadEndMultiple.ToString();
        }

        public void MinRoomChange(float value)
        {
            generator.minRooms = Mathf.RoundToInt(value);
            minRoomTxt.text = generator.minRooms.ToString();
        }
        
        public void HilbertCurveOrderChange(float value)
        {
            generator.hilbertCurveOrder = Mathf.RoundToInt(value);
            curveOrderTxt.text = generator.hilbertCurveOrder.ToString();
        }
        
        public void DeadEndMaxChange(float value)
        {
            generator.deadEndMax = Mathf.RoundToInt(value);
            deadMaxTxt.text = generator.deadEndMax.ToString();
        }
        
        public void DeadEndMultipleChange(float value)
        {
            generator.deadEndMultiple = Mathf.RoundToInt(value);
            deadMultipleTxt.text = generator.deadEndMultiple.ToString();
        }

        public void DoExitGame()
        {
            Application.Quit();
        }

        public void Reload()
        {
            SceneManager.LoadScene("GeneratedDungeon");
        }
    }
}
