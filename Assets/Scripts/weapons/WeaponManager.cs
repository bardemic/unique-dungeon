using System;
using System.Collections.Generic;
using generics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace weapons
{
    public class WeaponManager : MonoBehaviour
    {
        public GameObject currWeapon;
        private Hash128 _weaponHash;
        private float _timeCreated;
        public Dictionary<string, List<Sprite>> weaponSpriteDictionary = new Dictionary<string, List<Sprite>>();
        public List<string> weaponSpriteKeys;
        public List<Sprite> weaponSpriteValues;
        public int weaponSpriteKeysPerValue;
        [SerializeField] private float currentNewAbilityDifficulty = 10;
        public Vector2 newAbilityDifficultyMultiplierRange = new Vector2(2, 3);

        private Dictionary<string, Dictionary<string, int>> _weaponKillTracker =
            new Dictionary<string, Dictionary<string, int>>();

        private Camera _mainCam;
        private const float FollowRadius = 0.1f;
        private Animator _weaponAnimator;
        private static readonly int MouseX = Animator.StringToHash("mouseX");

        public void Start()
        {
            _weaponAnimator = currWeapon.GetComponent<Animator>();
        }

        public void Awake()
        {
            for (var i = 0; i < weaponSpriteValues.Count; i++)
            {
                if(weaponSpriteDictionary.ContainsKey(weaponSpriteKeys[i / weaponSpriteKeysPerValue]))
                    weaponSpriteDictionary[weaponSpriteKeys[i / weaponSpriteKeysPerValue]].Add(weaponSpriteValues[i]);
                else
                {
                    weaponSpriteDictionary.Add(weaponSpriteKeys[i/weaponSpriteKeysPerValue], new List<Sprite>());
                    weaponSpriteDictionary[weaponSpriteKeys[i / weaponSpriteKeysPerValue]].Add(weaponSpriteValues[i]);
                }
            }
            _mainCam = Camera.main;
        }

        public void Update()
        {
            FollowMouse();
        }

        private void FollowMouse()
        {
            var mousePosition = _mainCam.ScreenToWorldPoint(Input.mousePosition);
            var transform1 = transform;
            Vector2 dir;
            var parentPosition = transform1.parent.position;
            var pointVector2 = dir = mousePosition - parentPosition;
            pointVector2 = pointVector2.normalized;
            pointVector2 *= FollowRadius;
            pointVector2.y -= .1f;
            //transform1.localPosition = pointVector2;
            transform1.position = parentPosition + (Vector3)pointVector2;
            _weaponAnimator.SetFloat(MouseX, parentPosition.x - transform1.position.x);
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform1.rotation = Quaternion.AngleAxis(angle-90, Vector3.forward);
        }

        public void GenerateNewWeapon(string type)
        {
            _weaponHash = new Hash128();
            _weaponHash.Append(Time.time);
            _timeCreated = Time.time;
            _weaponHash.Append(type);
            _weaponAnimator = currWeapon.GetComponent<Animator>();
            // TODO change weapon to new prefab of weapon with given type
        }

        private void GiveCurrentWeaponNewHash()
        {
            var weaponComponent = currWeapon.GetComponent<Weapon>();
            _weaponHash = new Hash128();
            _weaponHash.Append(Time.time);
            _timeCreated = Time.time;
            _weaponHash.Append(weaponComponent.type);
            weaponComponent.weaponHash = _weaponHash.ToString();
        }

        public string GetHash()
        {
            return _weaponHash.ToString();
        }

        private void CreateNewAbility(string type)
        {
            var dealBonusDamage = Random.value > 0.5f;
            var dealDotDamage = Random.value > 0.5f;
            var weaponComponent = currWeapon.GetComponent<Weapon>();
            var dmg = Random.Range(0, weaponComponent.damagePoint[weaponComponent.weaponLevel]);
            var pushForce = dmg == 0 ? 
                Random.Range(weaponComponent.pushForce[weaponComponent.weaponLevel], weaponComponent.pushForce[weaponComponent.weaponLevel]*2) : 
                Random.Range(0, weaponComponent.pushForce[weaponComponent.weaponLevel]);

            if (dealBonusDamage && dealDotDamage && dmg > 2)
            {
                //Creates an ability with dot dmg and bonus dmg
                var ability = currWeapon.AddComponent<WeaponAbility>();
                var bonusDamage = UnityEngine.Random.Range(1, dmg - 1);
                var dotDamage = Random.Range(1, dmg - bonusDamage);
                var flatDamage = dmg - bonusDamage - dotDamage;
                var magicDamage = Random.Range(0, flatDamage);
            
                // Calculate base damage
                ability.damage = new Damage()
                {
                    damageAmount = flatDamage,
                    pushForce = pushForce,
                    magicDmg = magicDamage,
                    physicalDmg = flatDamage - magicDamage
                };
            
                // Calculate bonus damage
                magicDamage = Random.Range(0, magicDamage);
                ability.bonusDamage = new Damage()
                {
                    damageAmount = bonusDamage,
                    pushForce = 0,
                    magicDmg = magicDamage,
                    physicalDmg = bonusDamage - magicDamage
                };
            
                // Calculate dot damage
                if (dotDamage == 1)
                {
                    ability.dotDamage = 1;
                    ability.dotLength = 1;
                    magicDamage = Random.value > 0.5f ? 1 : 0;
                    ability.dotMagicDamage = magicDamage;
                    ability.dotPhysicalDamage = 1 - magicDamage;
                }
                else
                {
                    var length = Random.Range(1, dotDamage);
                    ability.dotLength = dotDamage % length == 0 ? length : 1;
                    ability.dotDamage = dotDamage / (int) ability.dotLength;
                }
                ability.enemyType = type;
            }
            else if (dealBonusDamage && dmg > 1)
            {
                //Creates an ability with at least bonus dmg
                var ability = currWeapon.AddComponent<WeaponAbility>();
                var bonusDamage = UnityEngine.Random.Range(1, dmg );
                var flatDamage = dmg - bonusDamage;
                var magicDamage = Random.Range(0, flatDamage);
            
                // Calculate base damage
                ability.damage = new Damage()
                {
                    damageAmount = flatDamage,
                    pushForce = pushForce,
                    magicDmg = magicDamage,
                    physicalDmg = flatDamage - magicDamage
                };
            
                // Calculate bonus damage
                magicDamage = Random.Range(0, magicDamage);
                ability.bonusDamage = new Damage()
                {
                    damageAmount = bonusDamage,
                    pushForce = 0,
                    magicDmg = magicDamage,
                    physicalDmg = bonusDamage - magicDamage
                };
            
                ability.enemyType = type;
            }
            else if (dealDotDamage && dmg > 1)
            {
                //Creates an ability with at least dot dmg
                var ability = currWeapon.AddComponent<WeaponAbility>();
                var dotDamage = Random.Range(1, dmg);
                var flatDamage = dmg - dotDamage;
                var magicDamage = Random.Range(0, flatDamage);
            
                // Calculate dot damage
                if (dotDamage == 1)
                {
                    ability.dotDamage = 1;
                    ability.dotLength = 1;
                    magicDamage = Random.value > 0.5f ? 1 : 0;
                    ability.dotMagicDamage = magicDamage;
                    ability.dotPhysicalDamage = 1 - magicDamage;
                }
                else
                {
                    var length = Random.Range(1, dotDamage);
                    ability.dotLength = dotDamage % length == 0 ? length : 1;
                    ability.dotDamage = dotDamage / (int) ability.dotLength;
                }
                ability.enemyType = type;
            }
            else
            {
                //Creates an ability with no bonus damage and no dot damage
                var ability = currWeapon.AddComponent<WeaponAbility>();
                var magicDamage = Random.Range(0, dmg);
            
                // Calculate base damage
                ability.damage = new Damage()
                {
                    damageAmount = dmg,
                    pushForce = pushForce,
                    magicDmg = magicDamage,
                    physicalDmg = dmg - magicDamage
                };
                ability.enemyType = type;
            }
        
        }

        public void KilledEnemy(string type)
        {
            var weaponComponent = currWeapon.GetComponent<Weapon>();
            if(weaponComponent.weaponHash == "")
                GiveCurrentWeaponNewHash();
            if (!_weaponKillTracker.ContainsKey(weaponComponent.weaponHash))
            {
                _weaponKillTracker.Add(weaponComponent.weaponHash, new Dictionary<string, int> {{type, 1}});
            }
            else
            {
                if (_weaponKillTracker[weaponComponent.weaponHash].ContainsKey(type))
                    _weaponKillTracker[weaponComponent.weaponHash][type] += 1;
                else
                    _weaponKillTracker[weaponComponent.weaponHash].Add(type,1);
            }

            if (!(_weaponKillTracker[weaponComponent.weaponHash][type] >= currentNewAbilityDifficulty)) return;
            CreateNewAbility(type);
            currentNewAbilityDifficulty *= Random.Range(newAbilityDifficultyMultiplierRange.x, newAbilityDifficultyMultiplierRange.y);
        }
    }
}
