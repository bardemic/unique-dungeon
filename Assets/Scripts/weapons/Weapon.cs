﻿using System.Collections.Generic;
using generics;
using scene_tools;
using UnityEngine;
using UnityEngine.Serialization;

namespace weapons
{
    public class Weapon : Collidable
    {
        // Damage struct
        public int[] damagePoint = {1,2,3};
        public float[] pushForce = {2f,2.1f,2.2f};

        public int weaponLevel = 0;
        private SpriteRenderer _spriteRenderer;
        public string weaponHash = "";
        [FormerlySerializedAs("weaponAbility")] public List<WeaponAbility> weaponAbilities = new List<WeaponAbility>();

        // swing
        private Animator _anim;
        public float cooldown = .5f;
        private float _lastSwing;
        public string type = "artillerySword";
        protected bool isRanged = false;
        protected int magicDmg = 0;
        protected int physicalDmg = 1;
        private AudioSource _swingSfx;
        private static readonly int Swing1 = Animator.StringToHash("swing");
        protected WeaponManager manager;

        private Dictionary<string, float> cooldownDictionary = new Dictionary<string, float>()
        {
            {"artillerySword", 1f}, {"civilianJian", 1.5f}, {"cleaver", 1.5f},{"dadao", 1.5f}, {"dagger", 1.5f},
            {"dragonSlayer", .5f}, {"hatchet", .5f}, {"jiuhuandao", 1f}, {"longHammer", .5f}, {"mace", 1f},
            {"magicSword", 1f}, {"rapier", 1.5f}, {"rustedSword", 1f}, {"shortHammer", 1f}, {"spear", 1.5f},
            {"spikedClub", 1.5f}, {"sword", 1f}, {"vikingSword", 1f}
        };

        private readonly Dictionary<float, float> _multiToDec = new Dictionary<float, float>()
        {
            {.5f, 1f}, {1f, .5f}, {1.5f, .5f}
        };

        private static readonly int Speed = Animator.StringToHash("speed");
        private static readonly int SwingUp = Animator.StringToHash("swingUp");

        protected void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected override void Start()
        {
            base.Start();
            _anim = GetComponent<Animator>();
            _swingSfx = GetComponent<AudioSource>();
            manager = GetComponentInParent<WeaponManager>();
            cooldown = _multiToDec[cooldownDictionary[type]];
            _anim.SetFloat(Speed, cooldownDictionary[type]);
        }

        protected override void Update()
        {
            base.Update();

            if (Input.GetMouseButtonDown(1))
            {
                if (!(Time.time - _lastSwing > cooldown)) return;
                _lastSwing = Time.time;
                Swing();
                Debug.Log("swing");
            }
        }

        protected override void OnCollide(Collider2D coll)
        {
            if (!coll.CompareTag("Fighter")) return;
            if (coll.CompareTag("Player")) return;
            
            // Create damage obj and send it
            var dmg = new Damage
            {
                damageAmount = damagePoint[weaponLevel],
                origin = transform.position,
                pushForce = pushForce[weaponLevel],
                magicDmg = magicDmg,
                physicalDmg = physicalDmg
            };
            
            coll.SendMessage("ReceivedDamage", dmg);
            foreach (var ability in weaponAbilities)
            {
                ability.OnHit(coll, transform.position);
            }
        }

        private void Swing()
        {
            //_anim.SetTrigger(isUp ? SwingUp : Swing1);
            _anim.SetTrigger(Swing1);
            //AnimateSwing(manager.weaponPos);
            _swingSfx.Play();
        }

        private void AnimateSwing(Vector2 pos)
        {
            if (pos.x > 0 && pos.y <= pos.x && pos.y > -pos.x) // Forward
            {
                _anim.Play("swing_forward");
            }
            else if (pos.y > 0 && pos.x < 0 && pos.y > -pos.x) // Up forward
            {
                _anim.Play("swing_up_forward");
            }
            else if (pos.y > 0 && pos.x >= 0 && pos.y > pos.x) // Up backward
            {
                _anim.Play("swing_up_backward");
            }
            else if (pos.x < 0 && pos.y <= pos.x && pos.y > -pos.x) // Backward
            {
                _anim.Play("swing_backward");
            }
            else if (pos.y < 0 && pos.x < 0 && pos.y > -pos.x) // down backward
            {
                _anim.Play("swing_down_backward");
            }
            else if (pos.y < 0 && pos.x >= 0 && pos.y > pos.x) // down forward
            {
                _anim.Play("swing_down_forward");
            }
        }

        public void UpgradeWeapon()
        {
            weaponLevel++;
            _spriteRenderer.sprite = GameManager.instance.WeaponSprites[weaponLevel];
        }

        public void SetWeaponLevel(int level)
        {
            weaponLevel=level;
            _spriteRenderer.sprite = GameManager.instance.WeaponSprites[weaponLevel];
        }

        public void NewAbility(string hash, WeaponAbility ability)
        {
            weaponHash = hash;
            weaponAbilities.Add(ability);
        }
    }
}