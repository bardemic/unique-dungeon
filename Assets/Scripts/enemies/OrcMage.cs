﻿using System;
using System.Collections;
using System.Collections.Generic;
using enemies.abilities;
using generics;
using scene_tools;
using UnityEngine;

namespace enemies
{
    public class OrcMage : Enemy
    {
        // Shield Data
        [SerializeField] private GameObject shieldGameObject;
        [SerializeField] private float shieldDuration = 2;
        [SerializeField] private float shieldKnockBack = .4f;
        [SerializeField] private float shieldCooldown = 3;
        [SerializeField] private float shieldCastCooldown = 5;
        private float _lastShield = 0;
        private CircleCollider2D _shieldCollider;
        
        // Projectile Data
        private float _lastShot = 0f;
        [SerializeField] private float shotCooldown = 1.5f;
        private List<OrcMageProjectile> _shots = new List<OrcMageProjectile>();
        public GameObject projectilePrefab;
        public GameObject projectileContainer;
        
        // Entity Data
        private Animator _anim;
        private static readonly int IsWalking = Animator.StringToHash("isWalking");
        private static readonly int WalkSpeed = Animator.StringToHash("walkSpeed");

        public OrcMage()
        {
            type = "orcMage";
            family = "orc";
        }
        
        protected override void Start()
        {
            base.Start();
            _lastShot = Time.time;
            _anim = GetComponent<Animator>();
            _anim.SetFloat(WalkSpeed, .5f);
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            _anim.SetBool(IsWalking, isWalking);
            if (!chasing) return;
            if (Time.time - _lastShot > shotCooldown)
            {
                _lastShot = Time.time;
                var shot = GetProjectile();
                shot.transform.position = transform.position;
                shot.targetPos = GameManager.instance.player.transform.position;
                shot.gameObject.SetActive(true);
            }

            if (Time.time - _lastShield > shieldDuration+shieldCastCooldown)
            {
                // Put the shield on cooldown
                _lastShield = Time.time;
                StartCoroutine(ActivateShield());
            }
        }

        protected override void ReceivedDamage(Damage dmg)
        {
            // If shield isn't active then take damage
            if(Time.time-_lastShield>shieldDuration)
                base.ReceivedDamage(dmg);
            
            // Push the player if they hit the shield while active
            if (shieldGameObject.activeSelf)
            {
                var push = new Damage
                {
                    damageAmount = 0,
                    origin = transform.position,
                    pushForce = shieldKnockBack,
                    magicDmg = 0,
                    physicalDmg = 0
                };
                GameManager.instance.player.SendMessage("ReceivedDamage", push);
            }
            
            // Activates the shield on damage
            if (Time.time - _lastShield > shieldDuration + shieldCooldown)
            {
                _lastShield = Time.time;
                StartCoroutine(ActivateShield());
            }
        }

        private IEnumerator ActivateShield()
        {
            var startTime = 0f;
            shieldGameObject.SetActive(true);
            while (startTime<shieldDuration)
            {
                startTime += Time.deltaTime;
                yield return null;
            }
            shieldGameObject.SetActive(false);
        }

        private OrcMageProjectile GetProjectile()
        {
            var shot = _shots.Find(t => !t.active);
            if (shot != null) 
                return shot;
            shot = Array.Find(projectileContainer.GetComponentsInChildren<OrcMageProjectile>(), t => !t.active);
            if (shot != null) 
                return shot;
            var shotGameObject = Instantiate(projectilePrefab, projectileContainer.transform, true);
            _shots.Add(shotGameObject.GetComponent<OrcMageProjectile>());
            shot = shotGameObject.GetComponent<OrcMageProjectile>();
            return shot;
        }
    }
}