﻿using System.Collections.Generic;
using enemies.abilities;
using generics;
using scene_tools;
using UnityEngine;

namespace enemies
{
    public class DemonBoss : Enemy
    {
        private float _lastShot = 0f;
        public float cooldown = 1.5f;
        public List<DemonBossProjectile> shots = new List<DemonBossProjectile>();
        public GameObject projectilePrefab;
        private SpriteRenderer _spriteRenderer;
        public int totalShots = 10;
        private Animator _anim;
        private static readonly int IsWalking = Animator.StringToHash("isWalking");
        private static readonly int WalkSpeed = Animator.StringToHash("walkSpeed");

        public DemonBoss()
        {
            type = "demonBoss";
            family = "demon";
        }

        public override void Generate(GenerationData data)
        {
            base.Generate(data);
            //TODO Vary damage from projectiles
        }

        protected override void Start()
        {
            base.Start();
            _lastShot = Time.time;
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _anim = GetComponent<Animator>();
            _anim.SetFloat(WalkSpeed, .5f);
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            _anim.SetBool(IsWalking, isWalking);
            if (chasing & (Time.time-_lastShot)>cooldown)
            {
                _lastShot = Time.time;
                var shot = GetProjectile();
                var position = transform.position;
                shot.transform.position = position;
                shot.targetPos = GameManager.instance.player.transform.position;
                shot.gameObject.SetActive(true);
            }
        }

        private DemonBossProjectile GetProjectile()
        {
            var shot = shots.Find(t => !t.active);
            if (shot == null && shots.Count < totalShots)
            {
                var shotGameObject = Instantiate(projectilePrefab, transform.parent.transform, true);

                shots.Add(shotGameObject.GetComponent<DemonBossProjectile>());
                shot = shotGameObject.GetComponent<DemonBossProjectile>();
            }
            else if (shot==null)
            {
                shot = shots[0];
                foreach (var s in shots)
                {
                    if (s.timeShot < shot.timeShot)
                        shot = s;
                }
                shot.ChangeActive(false);
            }

            return shot;
        }
    }
}