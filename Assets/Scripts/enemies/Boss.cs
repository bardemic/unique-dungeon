using System;
using generics;
using UnityEngine;

namespace enemies
{
    public class Boss : Enemy
    {
        public float[] fireBallSpeed = {2.5f, -2.5f};
        public Transform[] fireballs;
        public float distance = .35f;
    
        private void Update()
        {
            for (var i = 0; i< fireballs.Length; i++)
            {
                fireballs[i].position = transform.position + new Vector3((float) (-Math.Cos(Time.time * fireBallSpeed[i]) * distance),
                    (float) (Math.Sin(Time.time * fireBallSpeed[i]) * distance), 0);
            }
        
        }
    }
}
