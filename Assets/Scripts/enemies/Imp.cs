﻿using System.Threading.Tasks;
using generics;
using UnityEngine;

namespace enemies
{
    public class Imp : Enemy
    {
        private float origXspeed;
        private float origYspeed;
        private Animator anim;
        private static readonly int IsWalking = Animator.StringToHash("isWalking");

        public Imp()
        {
            type = "imp";
            family = "demon";
        }

        protected override void Start()
        {
            base.Start();
            origXspeed = xSpeed;
            origYspeed = ySpeed;
            anim = GetComponent<Animator>();
        }

        protected override void ReceivedDamage(Damage dmg)
        {
            dmg.pushForce *= 5;
            bool isIumme = !(Time.time - lastImmune > immuneTime);
            base.ReceivedDamage(dmg);
            if (!isIumme)
            {
                xSpeed += 4f;
                ySpeed += 4f;
                Invoke(nameof(RevertSpeed), 1.5f);
            }
        }

        private void RevertSpeed()
        {
            xSpeed = origXspeed;
            ySpeed = origYspeed;
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            anim.SetBool(IsWalking, isWalking);
        }
    }
}