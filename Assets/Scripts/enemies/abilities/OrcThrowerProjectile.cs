﻿using System.Collections;
using generics;
using scene_tools;
using UnityEngine;

namespace enemies.abilities
{
    public class OrcThrowerProjectile : Fireball
    {
        public Vector3 initTarget;
        private bool _changedDirection;
        
        protected override void FixedUpdate()
        {
            if (!gameObject.activeSelf) return;
            if (!_changedDirection)
            {
                // Travel to the first target and pause when getting there
                var delta = (initTarget - transform.position);
                if (Vector3.Distance(delta,Vector3.zero)<0.1f)
                {
                    _changedDirection = true;
                    targetPos = Vector3.zero;
                    StartCoroutine(PauseOnChange());
                    return;
                }
                transform.position += delta.normalized * moveSpeed.y * Time.deltaTime;
            }
            else
            {
                base.FixedUpdate();
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            _changedDirection = false;
        }

        private IEnumerator PauseOnChange()
        {
            var time = 0f;
            const float length = 1f;
            
            // After length of time target the player and move to that position
            while (time<length)
            {
                time += Time.deltaTime;
                if (time >= length)
                {
                    targetPos = GameManager.instance.player.transform.position; 
                    SetDirection();
                }
                yield return null;

            }
        }
    }
}