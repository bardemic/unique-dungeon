﻿using System;
using System.Collections;
using generics;
using scene_tools;
using UnityEngine;
using UnityEngine.Serialization;

namespace enemies.abilities
{
    public class OrcMageProjectile : Projectile
    {
        // Projectile data
        [SerializeField] private float lingering = 1f;
        private float _travelTime;
        private float _timeTraveled;
        [SerializeField] private float scaleChange = .5f;
        private bool _startedLingering;
        [SerializeField] private GameObject lingeringSprite;
        [SerializeField] private GameObject potionSprite;
        [SerializeField] private float lingeringDamageCooldown = 1;
        [SerializeField] private float lastDamaged = 0;
        private CircleCollider2D _lingeringCollider2D;
        
        // Damage data
        public int damage = 1;
        public float pushForce = 2;
        

        
        protected override void Start()
        {
            base.Start();
            disableOnStill = false;
            _lingeringCollider2D = lingeringSprite.GetComponent<CircleCollider2D>();
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            
            // Change sprite size while thrown and change to lingering on player hit
            if (_timeTraveled < _travelTime)
            {
                ChangeSpriteSize();
                var coll = HitPlayer();
                if (coll == null) return;
                _timeTraveled = _travelTime; 
                transform.localScale = Vector3.one;
            }

            // Start the lingering effect
            if (!_startedLingering)
            {
                targetPos = transform.position;
                _startedLingering = true;
                potionSprite.SetActive(false);
                lingeringSprite.SetActive(true);
                StartCoroutine(DisableAfterTime(lingering));
            }

            // Deal damage over time while the player is in the zone
            var collCircle = HitPlayer(_lingeringCollider2D);
            if (collCircle == null) return;
            if (Time.time - lastDamaged < lingeringDamageCooldown) return;
            lastDamaged = Time.time;
            var dmg = new Damage
            {
                damageAmount = damage,
                origin = transform.position,
                pushForce = pushForce,
                magicDmg = 0,
                physicalDmg = 0
            };
            
            collCircle.SendMessageUpwards("ReceivedDamage", dmg);
        }

        private IEnumerator DisableAfterTime(float length)
        {
            var time = 0f;
            while (time<length)
            {
                time += Time.deltaTime;
                if (time >= length)
                {
                    ChangeActive(false);
                }
                yield return null;
            }
            
        }

        private void ChangeSpriteSize()
        {
            // Increase for half and return to normal on the other half
            if (_timeTraveled < _travelTime / 2)
            {
                var scale = Mathf.Lerp(1, 1 + scaleChange, _timeTraveled / (_travelTime/2));
                transform.localScale = new Vector3(scale, scale, 1);
            }
            else
            {
                var scale = Mathf.Lerp(1 + scaleChange, 1, (_timeTraveled-_travelTime/2) / (_travelTime/2));
                transform.localScale = new Vector3(scale, scale, 1);
            }
            _timeTraveled += Time.deltaTime;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            // Reset the active children
            potionSprite.SetActive(true);
            lingeringSprite.SetActive(false);
            
            // Calculate time to get to target
            var travelDistance = Vector3.Distance(targetPos, transform.position);
            // TODO change speed to use both x and y
            _travelTime = travelDistance / moveSpeed.x;
        }
    }
}