﻿using System;
using generics;
using scene_tools;
using UnityEngine;

namespace enemies.abilities
{
    public class DemonBossProjectile : Projectile
    {
        private void Awake()
        {
            targetPos = GameManager.instance.player.transform.position;
            disableOnStill = false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            targetPos = GameManager.instance.player.transform.position;
        }
    }
}