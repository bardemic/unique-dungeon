﻿using System;
using System.Collections.Generic;
using generics;
using scene_tools;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.Tilemaps;

namespace enemies
{
    public class Caster : Enemy
    {
        public Tilemap grid;
        private float _lastShot = 0f;
        private float _lastTp = 0f;
        public float tpCooldown = 3f;
        public float cooldown = 1.5f;
        private List<Fireball> _shots = new List<Fireball>();
        public GameObject projectilePrefab;
        public GameObject projectileContainer;

        private List<Tuple<float, float>> _check = new List<Tuple<float, float>>();

        public Caster()
        {
            type = "caster";
        }

        public override void Generate(GenerationData data)
        {
            base.Generate(data);
            grid = data.grid;
        }

        protected override void Start()
        {
            chasing = true;
            base.Start();
            _lastShot = _lastTp = Time.time;
            runAtPlayer = false;
            _check.Add((new Tuple<float, float>(0,.25f)));
            _check.Add((new Tuple<float, float>(0.25f,.25f)));
            _check.Add((new Tuple<float, float>(0.25f,0f)));
            _check.Add((new Tuple<float, float>(-.25f,0f)));
            _check.Add((new Tuple<float, float>(-.25f,-.25f)));
            _check.Add((new Tuple<float, float>(0,-.25f)));
            _check.Add((new Tuple<float, float>(0.25f,-.25f)));
            _check.Add((new Tuple<float, float>(-0.25f,.25f)));
        }

        private void Update()
        {
            Teleport();
            Fire();
        }

        private void Fire()
        {
            if (!(chasing & Time.time - _lastShot > cooldown))
                return;
            var shot = GetProjectile();
            shot.transform.position = transform.position;
            shot.targetPos = GameManager.instance.player.transform.position;
            shot.SetDirection();
            shot.ChangeActive(true);
            _lastShot = Time.time;
        }
        
        private Fireball GetProjectile()
        {
            var shot = _shots.Find(t => !t.active);
            if (shot != null) 
                return shot;
            shot = Array.Find(projectileContainer.GetComponentsInChildren<Fireball>(), t => !t.active);
            if (shot != null) 
                return shot;
            var shotGameObject = Instantiate(projectilePrefab, projectileContainer.transform, true);
            _shots.Add(shotGameObject.GetComponent<Fireball>());
            shot = shotGameObject.GetComponent<Fireball>();
            return shot;
        }

        // Bug can go through walls if teleporting from right next to it
        private void Teleport()
        {
            if (!(chasing & Time.time - _lastTp > tpCooldown)) return;
            var pos = transform.position;
            var valid = new List<Vector3>();
            var playerPos = GameManager.instance.player.transform.position;
            foreach (var loc in _check)
            {
                var vec3 = new Vector3(pos.x + loc.Item1, pos.y + loc.Item2, 0);
                if(grid.HasTile(grid.WorldToCell(Vector3Int.RoundToInt(vec3)))& Vector3.Distance(pos, playerPos)<Vector3.Distance(vec3,playerPos))
                    valid.Add(vec3);
            }

            transform.position = valid.Count == 0 ? startingPosition : valid[UnityEngine.Random.Range(0, valid.Count)];
            _lastTp = Time.time;
        }
    }
}