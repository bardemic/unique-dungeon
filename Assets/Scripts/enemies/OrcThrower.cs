﻿using System;
using System.Collections.Generic;
using enemies.abilities;
using generics;
using scene_tools;
using UnityEngine;
using Random = UnityEngine.Random;

namespace enemies
{
    public class OrcThrower : Enemy
    {
        private float _lastShot = 0f;
        public float cooldown = 1.5f;
        private List<OrcThrowerProjectile> _shots = new List<OrcThrowerProjectile>();
        public GameObject projectilePrefab;
        public GameObject projectileContainer;
        private Animator _anim;
        private static readonly int IsWalking = Animator.StringToHash("isWalking");
        private static readonly int WalkSpeed = Animator.StringToHash("walkSpeed");

        public OrcThrower()
        {
            type = "orcThrower";
            family = "orc";
        }
        
        protected override void Start()
        {
            base.Start();
            _lastShot = Time.time;
            _anim = GetComponent<Animator>();
            _anim.SetFloat(WalkSpeed, .5f);
        }
        
        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            _anim.SetBool(IsWalking, isWalking);
            if (chasing & (Time.time-_lastShot)>cooldown)
            {
                _lastShot = Time.time;
                var shot = GetProjectile();
                var position = transform.position;
                shot.transform.position = position;
                shot.initTarget = new Vector3(position.x + Random.Range(-.5f, .5f), position.y + Random.Range(-.5f, .5f), 0);
                shot.gameObject.SetActive(true);
            }
        }
        
        private OrcThrowerProjectile GetProjectile()
        {
            var shot = _shots.Find(t => !t.active);
            if (shot != null) 
                return shot;
            shot = Array.Find(projectileContainer.GetComponentsInChildren<OrcThrowerProjectile>(), t => !t.active);
            if (shot != null) 
                return shot;
            var shotGameObject = Instantiate(projectilePrefab, projectileContainer.transform, true);
            _shots.Add(shotGameObject.GetComponent<OrcThrowerProjectile>());
            shot = shotGameObject.GetComponent<OrcThrowerProjectile>();
            return shot;
        }
    }
}