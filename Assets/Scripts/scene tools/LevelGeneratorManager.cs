﻿using System;
using System.Collections.Generic;
using System.Linq;
using generics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

namespace scene_tools
{
    public class LevelGeneratorManager : MonoBehaviour
    {
        [Range(3,20)] [SerializeField] public int hilbertCurveOrder = 4;
        [SerializeField] public int minRooms = 5;
        private readonly List<RoomMetaData> _roomPrefabs = new List<RoomMetaData>();
        public GameObject mapParent;
        [Range(0, 10)] [SerializeField] public int deadEndMultiple = 3;
        [Range(2, 40)] [SerializeField] public int deadEndMax = 10;
        private List<RoomMetaData> _generatedRooms;
        public List<EnemyPrefabs> enemyTypes;
        public List<string> enemyFamilies;
        public Vector2Int sampleSize = new Vector2Int(6, 6);
        
        // static curve data
        private static List<Point> _curve;
        private static int _curveOrder;
        
        public void Start()
        {
            FetchPrefabs();
            GenerateRooms();
            GenerateEnemies();
        }

        public void Generate()
        {
            FetchPrefabs();
            GenerateRooms();
        }


        #region Room generation

        private void FetchPrefabs()
        {
            var guids = Resources.LoadAll<GameObject>("Prefabs/Rooms");
            foreach (var guid in guids)
            {
                _roomPrefabs.Add(guid.GetComponent<RoomMetaData>());
            }
        }

        private List<Point> GetCurve()
        {
            // Reduce computing the curve everytime
            if (_curve == null || _curveOrder != hilbertCurveOrder)
            {
                _curve = GetPointsForCurve();
                _curveOrder = hilbertCurveOrder;
            }

            return _curve;
        }

        private class RoomType
        {
            public bool top, left, right, bottom;
            public readonly bool first;
            public bool last;
            private readonly List<RoomType> _connectedRooms = new List<RoomType>();
            public Tuple<int, int> pos;

            public const int Top = 0;
            public const int Left = 1;
            public const int Right = 2;
            public const int Bottom = 3;

            #region operators
            public override int GetHashCode()
            {
                return HashCode.Combine(top, left, right, bottom, first, last);
            }

            private bool Equals(RoomType other)
            {
                if (other is null) return false;
                return top == other.top && left == other.left && right == other.right && bottom == other.bottom;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((RoomType) obj);
            }

            public static bool operator == (RoomType a, RoomType b)
            {
                if (a is null && b is null) return true;
                if (a is null || b is null) return false;
                return a.Equals(b);
            }

            public static bool operator !=(RoomType a, RoomType b)
            {
                return !(a == b);
            }
            
            #endregion

            public RoomType()
            {
                
            }
            
            public RoomType(bool top, bool left, bool right, bool bottom)
            {
                this.top = top;
                this.left = left;
                this.right = right;
                this.bottom = bottom;
                first = last = false;
            }

            public RoomType(bool first)
            {
                this.first = first;
            }
            
            public void ConnectRooms(RoomType connected, int connectedSide)
            {
                if(_connectedRooms.Contains(connected)) return;
                switch (connectedSide)
                {
                    case Top:
                        top = true;
                        connected.bottom = true;
                        break;
                    case Left:
                        left = true;
                        connected.right = true;
                        break;
                    case Right:
                        right = true;
                        connected.left = true;
                        break;
                    case Bottom:
                        bottom = true;
                        connected.top = true;
                        break;
                }
                _connectedRooms.Add(connected);
                connected._connectedRooms.Add(connected);
            }
        }
        
        // Samples a region of the curve and creates roomType array from that sample
        private (List<RoomType>,RoomType[,]) SampleCurve()
        {
            // Clamp sample area
            if (sampleSize.x > (hilbertCurveOrder - 1) * 2)
                sampleSize.x = (hilbertCurveOrder - 1) * 2;
            else if (sampleSize.x < 2)
                sampleSize.x = 2;
            if (sampleSize.y > (hilbertCurveOrder - 1) * 2)
                sampleSize.y = (hilbertCurveOrder - 1) * 2;
            else if (sampleSize.y < 2)
                sampleSize.y = 2;
            
            var points = GetCurve();
            var rooms = new RoomType[sampleSize.x, sampleSize.y];
            var rx = Random.Range(0, 2 * hilbertCurveOrder - 1);
            var ry = Random.Range(0, 2 * hilbertCurveOrder - 1);
            var roomChains = new List<List<RoomType>>();
            var currentChain = new List<RoomType>();
            Tuple<int, int> prevRoomIndex = null;
            RoomType prevRoom = null;

            for (var i = 0; i < points.Count; i++)
            {
                // Check if point is outside the sample array
                if(points[i].x - rx < 0 || points[i].y - ry < 0 || 
                   points[i].x - rx >= sampleSize.x || points[i].y - ry >= sampleSize.y)
                    continue;

                //If its the first point in the sample
                if (prevRoom == null)
                {
                    prevRoom = rooms[points[i].x - rx, points[i].y - ry] = new RoomType(true);
                    prevRoom.pos = prevRoomIndex = new Tuple<int, int>(points[i].x - rx, points[i].y - ry);
                    currentChain = new List<RoomType>() {prevRoom};
                    roomChains.Add(currentChain);
                    continue;
                }

                var deltaX = points[i].x - rx - prevRoomIndex.Item1;
                var deltaY = points[i].y - ry - prevRoomIndex.Item2;
                //Check next room is adjacent to current room otherwise makes a new chain
                if (Mathf.Abs(deltaX) > 1 || Mathf.Abs(deltaY) > 1 && Mathf.Abs(Mathf.Abs(deltaX)-Mathf.Abs(deltaY))==1)
                {
                    prevRoom.last = true;
                    prevRoom = rooms[points[i].x - rx, points[i].y - ry] = new RoomType(true);
                    prevRoom.pos = prevRoomIndex = new Tuple<int, int>(points[i].x - rx, points[i].y - ry);
                    currentChain = new List<RoomType>() {prevRoom};
                    roomChains.Add(currentChain);
                    continue;
                }

                RoomType room;
                // Check which side the room is on
                switch (deltaX)
                {
                    //Check if not diagonal and then connect the rooms
                    case 1 when deltaY == 0:
                        room = new RoomType();
                        room.ConnectRooms(prevRoom, RoomType.Left);
                        break;
                    case -1 when deltaY == 0:
                        room = new RoomType();
                        room.ConnectRooms(prevRoom, RoomType.Right);
                        break;
                    case 0 when deltaY == 1:
                        room = new RoomType();
                        room.ConnectRooms(prevRoom, RoomType.Bottom);
                        break;
                    case 0 when deltaY == -1:
                        room = new RoomType();
                        room.ConnectRooms(prevRoom, RoomType.Top);
                        break;
                    default:
                        continue;
                }
                rooms[points[i].x - rx, points[i].y - ry] = prevRoom = room;
                prevRoom.pos = prevRoomIndex = new Tuple<int, int>(points[i].x - rx, points[i].y - ry);
                currentChain.Add(prevRoom);
            }

            if (prevRoom != null) prevRoom.last = true;

            // Find highest chain of rooms
            var highestChain = new List<RoomType>();
            foreach (var chain in roomChains.Where(chain => chain.Count > highestChain.Count))
            {
                highestChain = chain;
            }

            // Set rooms not in chain to an empty room
            for (var i = 0; i < rooms.GetLength(0); i++)
            {
                for (var j = 0; j < rooms.GetLength(1); j++)
                {
                    if (!highestChain.Exists(t => t.pos.Item1 == i && t.pos.Item2 == j))
                    {
                        rooms[i, j] = new RoomType();
                    }
                }
            }

            return (highestChain ,rooms);
        }

        private RoomType[,] AddDeadEnds(RoomType[,] rooms, ref List<RoomType> connected)
        {
            var adjacentRooms = new Tuple<int, int>[]
            {
                new(0, 1), new(1, 0), new(0, -1), new(-1, 0)
            };
            var r = deadEndMax - (connected.Count / deadEndMultiple) ;
            if (r < 2) r = 2;
            var blankRoom = new RoomType();
            for (var i = 0; i < r; i++)
            {
                for (var j = 0; j < 10; j++)
                {
                    // Find empty adjacent rooms for a random room
                    var index = Random.Range(0, connected.Count);
                    // Pick a new room if its the first or last in the chain
                    while (connected[index].first || connected[index].last)
                    {
                        index = Random.Range(0, connected.Count);
                    }
                    var randomRoom = connected[index];
                    var validAdjacent = (from adjacentRoom in adjacentRooms
                        where randomRoom.pos.Item1 + adjacentRoom.Item1>0 &&randomRoom.pos.Item1 + adjacentRoom.Item1<rooms.GetLength(0) &&
                              randomRoom.pos.Item2 + adjacentRoom.Item2>0 &&randomRoom.pos.Item2 + adjacentRoom.Item2<rooms.GetLength(1) && 
                              rooms[randomRoom.pos.Item1 + adjacentRoom.Item1,
                                  randomRoom.pos.Item2 + adjacentRoom.Item2] ==
                              blankRoom
                        select new Tuple<int, int>(randomRoom.pos.Item1 + adjacentRoom.Item1,
                            randomRoom.pos.Item2 + adjacentRoom.Item2)).ToList();
                    // If empty room adjacent then connect the rooms
                    if (validAdjacent.Count != 0)
                    {
                        var (adjacentX, adjacentY) = validAdjacent[Random.Range(0, validAdjacent.Count)];
                        
                        var deltaX = randomRoom.pos.Item1 - adjacentX;
                        var deltaY = randomRoom.pos.Item2 - adjacentY;
                        switch (deltaX)
                        {
                            //Check if not diagonal and then connect the rooms
                            case 1 when deltaY == 0:
                                randomRoom.ConnectRooms(rooms[adjacentX,adjacentY], RoomType.Left);
                                break;
                            case -1 when deltaY == 0:
                                randomRoom.ConnectRooms(rooms[adjacentX,adjacentY], RoomType.Right);
                                break;
                            case 0 when deltaY == 1:
                                randomRoom.ConnectRooms(rooms[adjacentX,adjacentY], RoomType.Bottom);
                                break;
                            case 0 when deltaY == -1:
                                randomRoom.ConnectRooms(rooms[adjacentX,adjacentY], RoomType.Top);
                                break;
                            default:
                                continue;
                        }
                    }
                    
                    // If it cant find a room after 10 tries stops adding dead ends
                    if (j == 9) return rooms;
                }
            }
            return rooms;
        }
        
        private void GenerateRooms()
        {
            var (connectedRooms, rooms) = SampleCurve();
            rooms = AddDeadEnds(rooms, ref connectedRooms);
            var tiles = RoomMetaData.AllTileTypes();
            var tileType = tiles[Random.Range(0, tiles.Count)];
            
            // Create new level if not enough rooms
            while (connectedRooms.Count<minRooms)
            {
                (connectedRooms, rooms) = SampleCurve();
                rooms = AddDeadEnds(rooms, ref connectedRooms);
            }

            // Generates map based on rooms array
            for (var i = 0; i < rooms.GetLength(0); i++)
            {
                for (var j = 0; j < rooms.GetLength(1); j++)
                {
                    var layout = new RoomMetaData.DoorLayout()
                    {
                        left = rooms[i, j].left,
                        right = rooms[i, j].right,
                        top = rooms[i,j].top,
                        bottom = rooms[i,j].bottom
                    };
                    // skip if empty
                    if(layout.Compare(new RoomMetaData.DoorLayout())) continue;
                    
                    //Get all matching prefabs
                    var validPrefabs = _roomPrefabs.FindAll(t =>
                            t.GetLayout().Compare(layout) && t.isRandom &&
                            t.isSpawn == rooms[i, j].first && t.isBoss == rooms[i,j].last);
                    
                    if (validPrefabs.Count == 0)
                    {
                        Debug.LogError($"Couldn't find room with matching layout. top:{layout.top} left:{layout.left} right:{layout.right} bottom:{layout.bottom}"
                        + $" spawn:{rooms[i,j].first} boss:{rooms[i,j].last}");
                    }
                    
                    // create the room
                    var r = Random.Range(0, validPrefabs.Count);
                    var newRoom = Instantiate(validPrefabs[r], mapParent.transform);
                    newRoom.pos = new Tuple<int, int>(i, j);
                    if (rooms[i, j].last)
                    {
                        newRoom.GetComponentInChildren<Portal>().sceneNames = new[] {"RestArea"}; // set boss portal
                    }
                    //ChangeTiles(newRoom.gameObject, newRoom.tileType, tileType);
                    _generatedRooms.Add(newRoom);
                    
                    //5.12 = 32 grid spaces
                    newRoom.transform.position = new Vector2(i * 5.12f, j * 5.12f);
                }
            }
        }
        #endregion

        #region Enemy Generation

        [Serializable]
        public struct EnemyPrefabs
        {
            public GameObject go;
            public EnemyPrefabData enemyPf;
        }
        
        private void GenerateEnemies()
        {
            var holder = new GameObject { name = "Enemies" };
            // TODO add enemy scaling per level using GenerationData
            foreach (var roomMeta in _generatedRooms.Where(t => t.mobSpawn))
            {
                //var grid = Array.Find(roomMeta.gameObject.GetComponents<Tilemap>(), t => t.name == "Valid teleport");
                var grid = roomMeta.spawnMap;
                var otherPositions = new List<Vector3>();
                var family = enemyFamilies[Random.Range(0, enemyFamilies.Count)];
                var enemies = enemyTypes.Where(t => t.enemyPf.enemy.family == "any" || t.enemyPf.enemy.family == family).ToArray();
                var parent = new GameObject { name = $"Room {roomMeta.pos.Item1},{roomMeta.pos.Item2} enemies", transform = { parent = holder.transform }};
                var totalEnemies = Random.Range(1, 5);

                for (var i = 0; i < totalEnemies; i++)
                {
                    // Get random valid position that hasn't already been used
                    var pos = new Vector3(Random.Range(-16, 16) * 0.16f, Random.Range(-16, 16) * 0.16f);
                    pos = grid.GetCellCenterLocal(grid.LocalToCell(pos));
                    var skip = 0;
                    if (otherPositions.Count == 0)
                        skip -= 30;
                    while (!otherPositions.Contains(pos) && !grid.HasTile(grid.LocalToCell(pos)) && skip < 20)
                    {
                        pos = new Vector3(Random.Range(-16, 16) * 0.16f, Random.Range(-16, 16) * 0.16f);
                        skip++;
                    }
                    if(skip >= 20)
                        break;
                    otherPositions.Add(pos);
                
                    // Create the enemy
                    var enemy = Instantiate(enemies[Random.Range(0, enemies.Length)].go, parent.transform);
                    var genData = new Enemy.GenerationData
                    {
                        xpValue = 1, 
                        grid = grid, 
                        hp = 5,
                        damage = 1,
                    };
                    enemy.GetComponentInChildren<Enemy>().Generate(genData);
                    enemy.transform.position = grid.LocalToWorld(pos);
                }
            }
        }

        #endregion
        
        #region Hilbert Curve

        private struct Point
        {
            private bool Equals(Point other)
            {
                return x == other.x && y == other.y;
            }

            public override bool Equals(object obj)
            {
                return obj is Point other && Equals(other);
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(x, y);
            }

            public int x, y;
            public static readonly Point Zero = new Point(0, 0);

            public Point(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
            public void Rot(int n, bool rx, bool ry)
            {
                if (ry) return;
                if (rx) {
                    x = (n - 1) - x;
                    y = (n - 1) - y;
                }
                Swap(ref x, ref y);
            }
 
            public override string ToString() {
                return $"({x}, {y})";
            }
            public static bool operator == (Point a, Point b)
            {
                return a.Equals(b) ;
            }

            public static bool operator !=(Point a, Point b)
            {
                return !(a == b);
            }
        }

        private static void Swap<T>(ref T a, ref T b) {
            (a, b) = (b, a);
        }

        private List<Point> GetPointsForCurve()
        {
            var n = 1 << hilbertCurveOrder;
            var points = new List<Point>();
            for (var d = 0; d < n * n; d++)
            {
                points.Add(FromD(n , d));
            }

            return points;
        }

        private static Point FromD(int n, int d)
        {
            var p = new Point(0, 0);
            var t = d;
            
            for (var s = 1; s < n; s <<= 1) {
                var rx = (t & 2) != 0;
                var ry = ((t ^ (rx ? 1 : 0)) & 1) != 0;
                p.Rot(s, rx, ry);
                p.x += rx ? s : 0;
                p.y += ry ? s : 0;
                t >>= 2;
            }
 
            return p;
        }

        #endregion
        
        
    }
}