using System.Collections;
using System.Collections.Generic;
using scene_tools;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMenu : MonoBehaviour
{
    // Test fields
    public Text levelText, hitpointText, coinsText, upgradeCostText, xpText;
    
    // Logic fields
    private int currentCharacterSelection = 0;
    public Image characterSelectionSprite;
    public Image weaponSprite;
    public RectTransform xpBar;
    
    // Character Selection
    public void OneArrowClick(bool right)
    {
        if (right)
        {
            currentCharacterSelection++;

            if (currentCharacterSelection == GameManager.instance.playerSprites.Count)
                currentCharacterSelection = 0;

            OnSelectionChanged();
        }
        else
        {
            currentCharacterSelection--;

            if (currentCharacterSelection == GameManager.instance.playerSprites.Count)
                currentCharacterSelection = GameManager.instance.playerSprites.Count-1;

            OnSelectionChanged();
        }
        
    }

    private void OnSelectionChanged()
    {
        characterSelectionSprite.sprite = GameManager.instance.playerSprites[currentCharacterSelection];
        GameManager.instance.player.SwapSprite(currentCharacterSelection);
    }

    // Weapon Upgrade
    public void OnUpgradeClick()
    {
        if(GameManager.instance.TryUpgradeWeapon())
            UpdateMenu();
    }
    
    // Update character information
    public void UpdateMenu()
    {
        // Weapon
        weaponSprite.sprite = GameManager.instance.WeaponSprites[GameManager.instance.weapon.weaponLevel];
        if(GameManager.instance.weapon.weaponLevel == GameManager.instance.weaponPrices.Count)
            upgradeCostText.text = "MAX";
        else
            upgradeCostText.text = GameManager.instance.weaponPrices[GameManager.instance.weapon.weaponLevel].ToString();
        
        // Meta
        hitpointText.text = GameManager.instance.player.hitPoint.ToString();
        coinsText.text = GameManager.instance.coins.ToString();
        levelText.text = GameManager.instance.GetCurrentLevel().ToString();
        
        // xp Bar
        int currlevel = GameManager.instance.GetCurrentLevel();
        if (currlevel == GameManager.instance.xpTable.Count)
        {
            xpText.text = GameManager.instance.experience.ToString() + " total experience points"; // Total xp
            xpBar.localScale = Vector3.one;
        }
        else
        {
            int prevLevelXp = GameManager.instance.GetXpToLevel(currlevel-1);
            int currLevelXp = GameManager.instance.GetXpToLevel(currlevel);

            int diff = currLevelXp - prevLevelXp;
            int currXpIntoLevel = GameManager.instance.experience - prevLevelXp;
            float progressRatio = currXpIntoLevel / (float) diff;
            xpBar.localScale = new Vector3(1-progressRatio, 1, 1);
            xpText.text = currXpIntoLevel.ToString() + " / " + diff;

        }
    }
}
