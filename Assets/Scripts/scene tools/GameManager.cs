using System.Collections.Generic;
using generics;
using UnityEngine;
using UnityEngine.SceneManagement;
using weapons;

namespace scene_tools
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                Destroy(player.gameObject);
                Destroy(floatingTextManager.gameObject);
                Destroy(hud);
                Destroy(menu);
                return;
            }
        
            PlayerPrefs.DeleteAll();
        
            instance = this;
            SceneManager.sceneLoaded += LoadState;
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
    
        // Resources
        public List<Sprite> playerSprites;
        public List<Sprite> WeaponSprites;
        public List<int> weaponPrices;
        public List<int> xpTable;

        // Reference
        public Player player;
        public FloatingTextManager floatingTextManager;
        public Weapon weapon;
        public WeaponManager weaponManager;
        public RectTransform hitpointBar;
        public GameObject hud;
        public GameObject menu;
        public Animator deathMenuAnim;
        public AudioManager audioManager;
        
    
        // Logic
        public int coins;
        public int experience;
        private static readonly int Hide = Animator.StringToHash("Hide");

        private void Start()
        {
            WeaponSprites = weaponManager.weaponSpriteDictionary[weaponManager.currWeapon.GetComponent<Weapon>().type];
        }

        public void ShowText(string msg, int fontSize, Color color, Vector3 position, Vector3 motion, float duration)
        {
            floatingTextManager.Show(msg, fontSize, color, position, motion, duration);
        }
    
        // Hitpoint Bar
        public void OnHitpointChange()
        {
            float ratio = (float) player.hitPoint / player.maxHitPoint;
            hitpointBar.localScale = new Vector3(1, ratio, 1);
        }
    
        // Upgrade Weapon
        public bool TryUpgradeWeapon()
        {
            if (weaponPrices.Count <= weapon.weaponLevel)
            {
                return false;
            }

            if (coins >= weaponPrices[weapon.weaponLevel])
            {
                coins -= weaponPrices[weapon.weaponLevel];
                weapon.UpgradeWeapon();
                return true;
            }

            return false;
        }
    
        // experience system
        public int GetCurrentLevel()
        {
            int r = 0;
            int add = 0;
            while (experience >= add)
            {
                add += xpTable[r];
                r++;
                if (r == xpTable.Count) // Max level
                    return r;
            }

            return r;
        }

        public void GrantXp(int xp)
        {
            int currLevel = GetCurrentLevel();
            experience += xp;

            if (currLevel < GetCurrentLevel())
                OnLevelUp();
        }

        public void OnLevelUp()
        {
            player.OnLevelUp();
        }

        public int GetXpToLevel(int level)
        {
            int r = 0;
            int xp = 0;

            while (r < level)
            {
                xp += xpTable[r];
                r++;
            }

            return xp;
        }
    
        // save state
        public void SaveState()
        {
            string s = "";
            s += "0" + "|";
            s += coins.ToString() + "|";
            s += experience.ToString() + "|";
            s += weapon.weaponLevel.ToString();
        
            PlayerPrefs.SetString("SaveState", s);
        }

        public void OnSceneLoaded(Scene s, LoadSceneMode mode)
        {
            player.transform.position = GameObject.Find("SpawnPoint").transform.position;
        }

        public void LoadState(Scene s, LoadSceneMode mode)
        {                   
            SceneManager.sceneLoaded -= LoadState;
            if(!PlayerPrefs.HasKey("SaveState"))
                return;
            string[] data = PlayerPrefs.GetString("SaveState").Split('|');

            coins = int.Parse(data[1]);
            experience = int.Parse(data[2]);
            player.SetLevel(GetCurrentLevel());
            weapon.SetWeaponLevel(int.Parse(data[3]));
        }
    
        // respawn
        public void Respawn()
        {
            deathMenuAnim.SetTrigger(Hide);
            UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
            player.Respawn();
        }
    

    }
}
