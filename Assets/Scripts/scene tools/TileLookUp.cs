﻿using System;
using System.Collections.Generic;
using generics;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace scene_tools
{
    [CreateAssetMenu(fileName = "FILENAME", menuName = "tile look up", order = 0)]
    public class TileLookUp : ScriptableObject
    {
        /*
         * Order of tiles
         * walls: Left, hole, middle, hole broken, right, goo
         * wall top: left, L right, middle, L left, other L right, other L left
         * wall+top: left, right, other left, other right
         * wall+top top: left, right, other right
         * vertical wall: right corner, right, right bottom, left corner, left, left bottom
         * door open: bottom left, top left, bottom right, top right, wall top left, wall top right, top left, top right
         * door closed: bottom left, top left, bottom right, top right
         * flag: blue, green, red, yellow
         * floor: blank, cracked BL, cracked TR, cracked TL, cracked BR, cracked small, cracked medium, cracked big, goo, ladder, stairs, hole
         * fountain: mana, health, top
         * pillar: top, mid, bottom, wall top, wall mid, wall bottom
         * short floor
         * spikes: none, 1, 2, 3
         */
        public List<TileBase> dessert, dungeon, smooth, jungle;
    }
}