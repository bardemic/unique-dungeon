﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingTextManager : MonoBehaviour
{
    public GameObject textContainer;
    public GameObject textPrefab;

    private List<FloatingText> floatingTexts = new List<FloatingText>();

    private FloatingText GetFloatingText()
    {
        FloatingText txt = floatingTexts.Find(t => !t.active);

        if (txt == null)
        {
            txt = new FloatingText();
            txt.go = Instantiate(textPrefab, textContainer.transform, true);
            txt.txt = txt.go.GetComponent<Text>();
            
            floatingTexts.Add(txt);
        }

        return txt;
    }

    public void Show(string msg, int fontSize, Color color,Vector3 position, Vector3 motion, float duration)
    {
        FloatingText floatingText = GetFloatingText();
        floatingText.txt.text = msg;
        floatingText.txt.fontSize = fontSize;
        floatingText.txt.color = color;

        floatingText.go.transform.position = position;
        //floatingText.go.transform.position = Camera.main.WorldToScreenPoint(position); // world space to screen space
        floatingText.motion = motion*0.016f;
        floatingText.duration = duration;
        floatingText.go.transform.localScale = new Vector3(0.016f, 0.016f, 0.016f);
        floatingText.Show();

    }

    private void Update()
    {
        foreach (var txt in floatingTexts)
        {
            txt.UpdateFloatingText();
        }
    }
}
