﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace scene_tools
{
    public class AudioManager : MonoBehaviour
    {
        public AudioSource nowPlaying;
        public static AudioManager instance;

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }

            instance = this;
            nowPlaying.Play();
            SceneManager.sceneLoaded += ChangeTrackOnLoad;
        }

        public void ChangeTrackOnLoad(Scene arg0, LoadSceneMode loadSceneMode)
        {
            ChangeTrack();
        }

        public void ChangeTrack()
        {
            var newTrack = Array.Find(FindObjectsOfType<AudioSource>(), t => t.CompareTag("backgroundMusic")&t!=nowPlaying);
            StopAllCoroutines();
            if(newTrack == null)
                nowPlaying.Play();
            else
                StartCoroutine(FadeTrack(newTrack));
        }

        public void ChangeTrack(AudioSource newTrack)
        {
            StopAllCoroutines();
            if(newTrack == null)
                nowPlaying.Play();
            else
                StartCoroutine(FadeTrack(newTrack));
        }

        public IEnumerator FadeTrack(AudioSource newTrack)
        {
            const float timeToFade = 2.25f;
            var timeElapsed = 0f;
            newTrack.Play();
            while (timeElapsed < timeToFade)
            {
                nowPlaying.volume = Mathf.Lerp(1, 0, timeElapsed / timeToFade);
                newTrack.volume = Mathf.Lerp(0, 1, timeElapsed / timeToFade);
                timeElapsed += Time.deltaTime;
                yield return null;
            }

            if (nowPlaying != newTrack)
            {
                nowPlaying.Stop();
                Destroy(nowPlaying.gameObject);
            }
                
            nowPlaying = newTrack;
        }
    }
}