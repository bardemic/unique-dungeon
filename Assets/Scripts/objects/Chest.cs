using System.Collections;
using System.Collections.Generic;
using scene_tools;
using UnityEngine;

public class Chest : Collectable
{
    public Sprite emptyChest;
    public int coinAmount = 5;
    protected override void OnCollect()
    {
        if (!collected)
        {
            collected = true;
            GetComponent<SpriteRenderer>().sprite = emptyChest;
            GameManager.instance.coins += coinAmount;
            GameManager.instance.ShowText("+"+coinAmount+" Coins", 35, Color.yellow, transform.position, Vector3.up * 50, 1f);
        }
    }
}
