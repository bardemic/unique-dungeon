﻿using System;
using generics;
using scene_tools;
using UnityEditor;
using UnityEngine;
using Random = System.Random;


public class Coin : Mover
{
    private ContactFilter2D filter;
    private Collider2D[] hits = new Collider2D[10];
    public float dontCollect = .3f;
    private float created;
    private static readonly int Start1 = Animator.StringToHash("start");
    private Animator anim;

    // Update is called once per frame
    protected virtual void Update()
    {
        // Collision work
        boxCollider.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i]==null)
                continue;
            OnCollide(hits[i]);
            
            // clean up
            hits[i] = null;
        }
    }
    private void Awake()
    {
        pushDirection = new Vector3(UnityEngine.Random.Range(-.7f, .7f), UnityEngine.Random.Range(-.7f, .7f), 0);
        created = Time.time;
        anim = GetComponent<Animator>();
        anim.SetFloat(Start1,UnityEngine.Random.Range(0f, .4f));
    }

    protected override void ReceivedDamage(Damage dmg)
    {
        return;
    }

    private void FixedUpdate()
    {
        UpdateMotor(Vector3.zero);
    }
    
    private void OnCollide(Collider2D coll)
    {
        if (coll.CompareTag("Player") & (Time.time - created)>dontCollect)
        {
            GameManager.instance.coins += 1;
            GameManager.instance.ShowText("+1 Coin", 35, Color.yellow, transform.position, Vector3.up * 50, 1f);
            Destroy(gameObject);
        }
    }
}
