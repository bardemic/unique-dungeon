using System;
using System.Collections;
using System.Collections.Generic;
using generics;
using scene_tools;
using UnityEngine;

public class Player : Mover
{
    private SpriteRenderer _spriteRenderer;
    private static readonly int Show = Animator.StringToHash("Show");
    private bool _isAlive = true;
    public GameObject playerSpriteObj;
    private Animator _spriteAnimator;
    private static readonly int IsWalking = Animator.StringToHash("isWalking");
    private static readonly int Jump = Animator.StringToHash("jump");

    protected override void Start()
    {
        base.Start();
        _spriteRenderer = playerSpriteObj.GetComponent<SpriteRenderer>();
        //boxCollider = playerSpriteObj.GetComponent<BoxCollider2D>();
        _spriteAnimator = playerSpriteObj.GetComponent<Animator>();
    }

    protected void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
            _spriteAnimator.SetTrigger(Jump);
    }

    private void FixedUpdate()
    {
        if (!_isAlive)
            return;
        float x = Input.GetAxisRaw("Horizontal"); 
        float y = Input.GetAxisRaw("Vertical");
        if (x!=0 || y!=0)
            _spriteAnimator.SetBool(IsWalking, true);
        else
            _spriteAnimator.SetBool(IsWalking, false);
        UpdateMotor(new Vector3(x, y, 0));
    }

    protected override void FlipSprite(Vector3 moveDeltaVector3)
    {
        if (moveDeltaVector3.x > 0)
            _spriteRenderer.flipX = false;
        else if (moveDeltaVector3.x < 0)
            _spriteRenderer.flipX = true;
    }

    public void SwapSprite(int skinID)
    {
        _spriteRenderer.sprite = GameManager.instance.playerSprites[skinID];
    }

    public void OnLevelUp()
    {
        maxHitPoint++;
        hitPoint = maxHitPoint;
        GameManager.instance.OnHitpointChange();
    }

    public void SetLevel(int level)
    {
        for(var i = 0; i < level; i++)
            OnLevelUp();
        // Bug tries to fix when player levels up and dies on the same frame they get stuck
        _isAlive = true;
    }

    public void Heal(int healingAmount)
    {
        if (hitPoint== maxHitPoint)
            return;
        hitPoint += healingAmount;
        GameManager.instance.ShowText("+"+healingAmount.ToString()+" hp", 30, Color.green, transform.position, Vector3.up*30,1f );
        if (hitPoint > maxHitPoint)
            hitPoint = maxHitPoint;
        GameManager.instance.OnHitpointChange();
    }

    protected override void ReceivedDamage(Damage dmg)
    {
        if(!_isAlive)
            return;
        base.ReceivedDamage(dmg);
        GameManager.instance.OnHitpointChange();
    }

    protected override void Death()
    {
        GameManager.instance.deathMenuAnim.SetTrigger(Show);
        _isAlive = false;
    }

    public void Respawn()
    {
        _isAlive = true;
        Heal(maxHitPoint);
        lastImmune = Time.time;
        pushDirection = Vector3.zero;
    }
}
